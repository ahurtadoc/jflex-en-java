package codigo;
import static codigo.Tokens.*;
%%
%class Lexer
%type Tokens
L=[a-zA-Z_]+
D=[0-9]+
F=[0-9][0-9]*"."[0-9]+
espacio=[ |\n]+

%{
    public String lexeme;
%}
%%

if | else | while | for | null | this | super | Public | public | Private | private 
| Protected | protected | final | Transfert | Volatile | return | class | exit
| System | out | print | println | void | main | package | import | extends | implements 
| new | abstract | assert | break | case | catch | continue | default | do | enum | exports 
| finally | instanceof | interface | module | native | requires | switch | synchronized | throw
| throws | transient | try | volatile | true | false | var
{lexeme=yytext(); return Reservadas;}

byte | int | char | String | Char | Integer | boolean | Boolean | Double | double | Static | float
| long | short | static {lexeme=yytext(); return Tipos_Datos;}

"+" | "-" | "*" | "/" | "=" {lexeme=yytext(); return Tipos_Operador_Matematico;}

{espacio} {return blanco}

"%" |"<" | ">" | "&" | "|" {lexeme=yytext(); return Tipo_Operador_Logico;}

"." | "," | "(" | ")" | "[" | "]" | "{" | "}" | "'" | "@" | "#" {lexeme=yytext(); return Tipo_Operador_Lexico;}

{espacio} {/*Ignore*/}
"//".* {/*Ignore*/}

";" {return Punto_Coma;}

{L}({L}|{D})* | "_" {lexeme=yytext(); return Identificador;}
("(-"{D}+")")|{D}+ {lexeme=yytext(); return Numero_Entero;}
("(-"{F}+")")|{F}+ {lexeme=yytext(); return Numero_Flotante;}
 . {return ERROR;}