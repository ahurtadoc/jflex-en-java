
package codigo;

public enum Tokens {
    /*Aqui devolvemos los valores, para poder pasarlo al Panel.java*/
    Reservadas, Identificador,Tipos_Datos,Numero_Entero,Numero_Flotante,Tipo_Operador_Logico,Tipos_Operador_Matematico,blanco,Punto_Coma,ERROR,Tipo_Operador_Lexico;
}
