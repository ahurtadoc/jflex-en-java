##Analizador Léxico (Java)
Alumno:
-	Aron Hurtado Cruz
-	Elmerson Portugal Carpio
Tema:
-	Analizador Léxico
Lenguaje Programado y Evaluado:
-	Java 


##Resumen
Lo que se busca es poder identificar cada palabra del código de Java, para poder realizar dicho ejerció, recibimos ayuda del “JFlex.jar”, con esta herramienta podemos, identificar mucho más rápido el lenguaje, ya que importa las librerías necesarias para realizar dicha operación. 
##División del Software:
Para realizar dicha operación hemos dividido, software, en tres partes muy importantes, el Lexer.flex, Tokens.java y Panel.java

	Lexer.flex
		En esta parte del código podemos encontrar, en palabras simples, el diccionario que nos permitirá identificar las palabras del código
			/*
			int | char | String | Char | Int | boolean | Boolean | Double | double | Static 
			{lexeme=yytext(); return Tipos_Datos;}
			*/
		Como se puede ver, especificamos una palabra reservada “int” le decimos que si encuentra palabra nos devuelva “Tipos_Datos”.


	Tokens.java 
		En esta parte del código nos permite tomar el valor y devolverlo “seria como pasarlo del. flex al .java” se podría decir que es como un traductor, un intermediario 
			/*
			public enum Tokens {
    			Reservadas, Identificador,Tipos_Datos,Numero,Tipo_Simbolo,Tipos_Operador, blanco,ERROR,;
			}
			*/

	Panel.java 
		No hay más que decir, dicha clase está encargada de la interfaz gráfica, aquí hacemos la implementación poder hacer funcionar los botones, mostrar el resultado. 
		Aquí le damos una especificación a los “return” y le pedimos que devuelva un “String”
			/*
			case ERROR:
                        resultado += lexer.lexeme + "el símbolo no identificado\n";
                        break;
			*/
##Ejemplo 
		Si podemos en el Jtext la palabra:
			Public -> Te dará como respuesta “Es una palabra reservada”

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

