package codigo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader; 
import java.util.logging.Level;
import java.util.logging.Logger;

public class Panel extends javax.swing.JFrame {

    public Panel() {
        initComponents();
        // Nos permite centrar el Panel
        setLocationRelativeTo(null);
    }
    public void borar(){
        txtEntrada.setText("");
        txtSalida.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Analizar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtSalida = new javax.swing.JTextArea();
        Limpiar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtEntrada = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Analizar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        Analizar.setText("Analizar");
        Analizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnalizarActionPerformed(evt);
            }
        });

        txtSalida.setColumns(20);
        txtSalida.setFont(new java.awt.Font("MS Reference Sans Serif", 0, 14)); // NOI18N
        txtSalida.setRows(5);
        jScrollPane1.setViewportView(txtSalida);

        Limpiar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        Limpiar.setText("Limpiar");
        Limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimpiarActionPerformed(evt);
            }
        });

        txtEntrada.setColumns(20);
        txtEntrada.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        txtEntrada.setRows(5);
        jScrollPane2.setViewportView(txtEntrada);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addGap(45, 45, 45)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Analizar)
                    .addComponent(Limpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(54, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Analizar)
                        .addGap(34, 34, 34)
                        .addComponent(Limpiar))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AnalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnalizarActionPerformed
        // TODO add your handling code here:
        File archivo = new File("archivo.txt");
        // NO va permitir en el docuemnto
        PrintWriter escribir;

        try {
            escribir = new PrintWriter(archivo);
            escribir.print(txtEntrada.getText());
            escribir.close();
        } 
        catch (FileNotFoundException ex) {
            Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            Reader lector = new BufferedReader(new FileReader("archivo.txt"));
            Lexer lexer = new Lexer(lector);
            String resultado = "";
            
            /*Aqui emepieza el recorrido de los valores*/
            while (true) {
                
                Tokens tokens = lexer.yylex();
                /*Aqui se muestra el valor por consola*/
                System.out.println(tokens);
                if (tokens == null) {
                    txtSalida.setText(resultado);
                    return;
                }
                
                /*Vamos a identificar los valores y devolver el respuesta*/
            switch (tokens) {
                /*ERROR -> palabra reservada*/
                    case ERROR:
                        /*Esto se muestra en el JText*/
                        resultado += lexer.lexeme + " el simbolo no esta identificado\n";
                        break;
                /*Identificador -> palabra reservada*/
                    case Identificador:
                        resultado += lexer.lexeme + " : No es una palabra reservada \n";
                        break;
                /*Reservadas -> palabra reservada*/
                    case Reservadas:
                        resultado += lexer.lexeme + " : Es una palabra reservada \n";
                        break;
                /*Tipos_Datos -> palabra reservada*/       
                    case Tipos_Datos:
                        resultado += lexer.lexeme + " : Es un tipo de dato.\n";
                        break;
                /*Tipos_Operador_Matematico -> palabra reservada*/       
                    case Tipos_Operador_Matematico:
                        resultado += lexer.lexeme + " : Es un tipo de operador matemático.\n";
                        break;
                /*Tipo_Operador_Logico -> palabra reservada*/   
                    case Tipo_Operador_Logico:
                        resultado += lexer.lexeme + " : Es un tipo de operador lógico.\n";
                        break;
                /*Tipo_Operador_Lexico -> palabra reservada*/       
                    case Tipo_Operador_Lexico:
                        resultado += lexer.lexeme + " : Gramatica de lenguaje de programación.\n";
                        break;
                /*Numero_Entero -> palabra reservada*/
                    case Numero_Entero:
                        resultado += lexer.lexeme + " : Es un numero entero.\n";
                        break;      
                /*Numero_Flotante -> palabra reservada*/        
                    case Numero_Flotante:
                        resultado += lexer.lexeme + " : Es un numero flotante.\n";
                        break;                          
                /*Punto_Coma -> palabra reservada*/        
                    case Punto_Coma:
                        resultado += " ; : Fin de una sentencia.\n";
                        break;
                 /*blanco -> palabra reservada*/       
                    case blanco:
                        resultado += " Espacio en blanco \n";
                        break;                        
                }
            }
            
        }
        catch (FileNotFoundException ex) {
            Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (IOException ex) {
            Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_AnalizarActionPerformed

    private void LimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LimpiarActionPerformed
        borar();
    }//GEN-LAST:event_LimpiarActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Panel().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Analizar;
    private javax.swing.JButton Limpiar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea txtEntrada;
    private javax.swing.JTextArea txtSalida;
    // End of variables declaration//GEN-END:variables
}
